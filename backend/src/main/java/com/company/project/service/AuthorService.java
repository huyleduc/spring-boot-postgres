package com.company.project.service;


import com.company.project.entity.Author;
import com.company.project.entity.dto.AuthorDTO;
import com.company.project.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    public Author createAuthor(AuthorDTO authorDTO) {
        Author author = new Author(authorDTO.firstName(), authorDTO.lastName(), authorDTO.birthYear());
        return authorRepository.save(author);
    }

    public List<Author> getAllAuthors() {
        return  (List<Author>) authorRepository.findAll();
    }

    public Author getAuthorById(Long id) {
        Optional<Author> authorOptional = authorRepository.findById(id);
        return authorOptional.orElse(null);
        // Return null or throw an exception based on your application requirements
    }

    public Author updateAuthor(Long id, AuthorDTO authorDTO) {
        return authorRepository.findById(id)
                .map(author -> {
                    authorDTO.getFirstNameOptional().ifPresent(author::setFirstName);
                    authorDTO.getLastNameOptional().ifPresent(author::setLastName);
                    authorDTO.getBirthYearOptional().ifPresent(author::setBirthYear);
//                    return convertToDto(authorRepository.save(author));
                    return authorRepository.save(author);
                })
                .orElse(null);
    }

    public boolean deleteAuthorById(Long id) {
        if (authorRepository.existsById(id)) {
            authorRepository.deleteById(id);
            return true;
        }
        return false;
    }


}