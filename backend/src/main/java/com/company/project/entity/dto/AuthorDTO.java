package com.company.project.entity.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Optional;

public record AuthorDTO(
        String firstName,
        String lastName,
        int birthYear
) {
    // Empty record body
    @JsonIgnore
    public Optional<String> getFirstNameOptional() {
        return Optional.ofNullable(firstName);
    }

    @JsonIgnore
    public Optional<String> getLastNameOptional() {
        return Optional.ofNullable(lastName);
    }

    @JsonIgnore
    public Optional<Integer> getBirthYearOptional() {
        return Optional.ofNullable(birthYear);
    }
}