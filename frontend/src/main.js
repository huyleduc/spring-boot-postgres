import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueAxios from "vue-axios";
import axiosInstance from "./api/axios";

const app = createApp(App);

app.use(router)
app.use(VueAxios,axiosInstance)
app.config.globalProperties.$axios = axiosInstance;
app.mount("#app");